package purificiency.input;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import purificiency.camera.Camera;
import purificiency.display.PGameWindow;

public class InputHandler {

	static boolean released = false;
	
	public static void update(int delta) {
		
		float crx = (float) Math.toRadians(Camera.getRX());
		float cry = (float) Math.toRadians(Camera.getRY());
		
		float cos = (float) Math.cos(cry);
		float sin = (float) Math.sin(cry);
		
		boolean keyw = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keya = Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keys = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyd = Keyboard.isKeyDown(Keyboard.KEY_D);
		
		if (keyw && keya) {
			Camera.translate((float) Math.sin(Math.toRadians(Camera.getRY()-45.0f))*delta*0.01f, 0.0f, (float) -Math.cos(Math.toRadians(Camera.getRY()-45.0f))*delta*0.01f);
		} else if (keya && keys) {
			Camera.translate((float) -Math.sin(Math.toRadians(Camera.getRY()+45.0f))*delta*0.01f, 0.0f, (float) Math.cos(Math.toRadians(Camera.getRY()+45.0f))*delta*0.01f);
		} else if (keys && keyd) {
			Camera.translate((float) -Math.sin(Math.toRadians(Camera.getRY()-45.0f))*delta*0.01f, 0.0f, (float) Math.cos(Math.toRadians(Camera.getRY()-45.0f))*delta*0.01f);
		} else if (keyd && keyw) {
			Camera.translate((float) Math.sin(Math.toRadians(Camera.getRY()+45.0f))*delta*0.01f, 0.0f, (float) -Math.cos(Math.toRadians(Camera.getRY()+45.0f))*delta*0.01f);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			boolean collisionf = false;
			
			// Front walls
			for (int i = 0; i<PGameWindow.currentMap.frontWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.frontWallTiles.get(i);
				int wally = PGameWindow.currentMap.frontWallTiles.get(i+1);
				if (-Camera.getZ()+cos*delta*0.01f+0.25f >= wally && -Camera.getZ()+cos*delta*0.01f <= wally+0.5f) { // if future movement is bigger than walls y pos
					if (Camera.getX() >= wallx && Camera.getX() <= wallx+2)
						collisionf = true;
				}
			}

			boolean collisionl = false;
			// Left walls			
			for (int i = 0; i<PGameWindow.currentMap.leftWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.leftWallTiles.get(i);
				int wally = PGameWindow.currentMap.leftWallTiles.get(i+1);
				if (Camera.getX()+sin*delta*0.01f+0.25f >= wallx && Camera.getX()+sin*delta*0.01f <= wallx+0.5f) {
					if (-Camera.getZ() >= wally-2.0f && -Camera.getZ() <= wally)
						collisionl = true;
				}
			}
			
			boolean collisionb = false;
			// Front walls
			for (int i = 0; i<PGameWindow.currentMap.backWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.backWallTiles.get(i);
				int wally = PGameWindow.currentMap.backWallTiles.get(i+1);
				if (-Camera.getZ()+cos*delta*0.01f+0.25f >= wally && -Camera.getZ()+cos*delta*0.01f <= wally+0.5f) { // if future movement is bigger than walls y pos
					if (Camera.getX() >= wallx && Camera.getX() <= wallx+2.0f)
						collisionb = true;
				}
			}
			

			boolean collisionr = false;
			// Left walls			
			for (int i = 0; i<PGameWindow.currentMap.rightWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.rightWallTiles.get(i);
				int wally = PGameWindow.currentMap.rightWallTiles.get(i+1);
				if (Camera.getX()+sin*delta*0.01f+0.25f >= wallx && Camera.getX()+sin*delta*0.01f <= wallx+0.5f) {
					if (-Camera.getZ() >= wally-2.0f && -Camera.getZ() <= wally)
						collisionr = true;
				}
			}
			
			if (collisionf && collisionl && collisionb) {
				Camera.translate(0.0f, 0.0f, 0.0f);
			} else {
				if (!collisionf && !collisionb)
					Camera.translate(0.0f, 0.0f, -cos*delta*0.01f);
				if (!collisionl && !collisionr)
					Camera.translate(sin*delta*0.01f, 0.0f, 0.0f);	
			}
		} else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			boolean collisionf = false;
			
			// Front walls
			for (int i = 0; i<PGameWindow.currentMap.frontWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.frontWallTiles.get(i);
				int wally = PGameWindow.currentMap.frontWallTiles.get(i+1);
				if (-Camera.getZ()-sin*delta*0.01f+0.25f >= wally && -Camera.getZ()-sin*delta*0.01f <= wally+0.5f) { // if future movement is bigger than walls y pos
					if (Camera.getX() >= wallx && Camera.getX() <= wallx+2)
						collisionf = true;
				}
			}

			boolean collisionl = false;
			// Left walls			
			for (int i = 0; i<PGameWindow.currentMap.leftWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.leftWallTiles.get(i);
				int wally = PGameWindow.currentMap.leftWallTiles.get(i+1);
				if (Camera.getX()-cos*delta*0.01f+0.25f >= wallx && Camera.getX()-cos*delta*0.01f <= wallx+0.5f) {
					if (-Camera.getZ() >= wally-2.0f && -Camera.getZ() <= wally)
						collisionl = true;
				}
			}
			
			boolean collisionb = false;
			// Front walls
			for (int i = 0; i<PGameWindow.currentMap.backWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.backWallTiles.get(i);
				int wally = PGameWindow.currentMap.backWallTiles.get(i+1);
				if (-Camera.getZ()-sin*delta*0.01f+0.25f >= wally && -Camera.getZ()-sin*delta*0.01f <= wally+0.5f) { // if future movement is bigger than walls y pos
					if (Camera.getX() >= wallx && Camera.getX() <= wallx+2.0f)
						collisionb = true;
				}
			}
			

			boolean collisionr = false;
			// Left walls			
			for (int i = 0; i<PGameWindow.currentMap.rightWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.rightWallTiles.get(i);
				int wally = PGameWindow.currentMap.rightWallTiles.get(i+1);
				if (Camera.getX()-cos*delta*0.01f+0.25f >= wallx && Camera.getX()-cos*delta*0.01f <= wallx+0.5f) {
					if (-Camera.getZ() >= wally-2.0f && -Camera.getZ() <= wally)
						collisionr = true;
				}
			}
			
			if (collisionf && collisionl && collisionb) {
				Camera.translate(0.0f, 0.0f, 0.0f);
			} else {
				if (!collisionf && !collisionb)
					Camera.translate(0.0f, 0.0f, -sin*delta*0.01f);
				if (!collisionl && !collisionr)
					Camera.translate(-cos*delta*0.01f, 0.0f, 0.0f);	
			}
			//Camera.translate(-cos*delta*0.01f, 0.0f, -sin*delta*0.01f);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			boolean collision = false;
			for (int i = 0; i<PGameWindow.currentMap.frontWallTiles.size(); i+=2) {
				int wallx = PGameWindow.currentMap.frontWallTiles.get(i);
				int wally = PGameWindow.currentMap.frontWallTiles.get(i+1);
				if (-Camera.getZ()-cos*delta*0.01f+0.25f > wally) { // if future movement is bigger than walls y pos
					if (Camera.getX() > wallx && Camera.getX() < wallx+2)
						collision = true;
				}
			}
			if (!collision) // if collision didnt happuuun
				Camera.translate(-sin*delta*0.01f, 0.0f, cos*delta*0.01f);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			Camera.translate(cos*delta*0.01f, 0.0f, sin*delta*0.01f);
		}
		if (Mouse.isButtonDown(0)) {
			Camera.rotate(-Mouse.getDY()/10.0f, Mouse.getDX()/10.0f, 0.0f);
			Mouse.setGrabbed(true);
		} else {
			Mouse.setGrabbed(false);
		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			if (!released) {
				released = true;
				PGameWindow.currentMap.placelight =! PGameWindow.currentMap.placelight;
			}
		} else {
			released = false;
		}
		while (Keyboard.next()) {
			if (Keyboard.getEventKey() == Keyboard.KEY_L) {
				if (!Keyboard.getEventKeyState())
					PGameWindow.currentMap.bleedlight =! PGameWindow.currentMap.bleedlight;
			}
		}
	}
}
