package purificiency.map;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;

import purificiency.camera.Camera;
import purificiency.display.PGameWindow;

public class MapLoader {
	
	static BufferedImage currentMap;
	
	public static void render() {
		/*
		for (int x = 0; x<currentMap.getWidth(); x++) {
			for (int y = 0; y<currentMap.getHeight(); y++) {
				
				Color currentColor = new Color(currentMap.getRGB(x, y), true);
				
				if (currentColor.getAlpha() != 0) {
					System.out.println(currentColor.getRed());
					GL11.glColor3f(currentColor.getRed()/255.0f, currentColor.getGreen()/255.0f, currentColor.getBlue()/255.0f);
					GL11.glPushMatrix();
					GL11.glRotatef(Camera.getRX(), 1.0f, 0.0f, 0.0f);
					GL11.glRotatef(Camera.getRY(), 0.0f, 1.0f, 0.0f);
					GL11.glRotatef(Camera.getRZ(), 0.0f, 0.0f, 1.0f);
					GL11.glTranslatef(-Camera.getX(), -Camera.getY(), -Camera.getZ());
					GL11.glBegin(GL11.GL_QUADS);
						GL11.glVertex3f(y*2.0f, -2.0f, -x*2.0f);
						GL11.glVertex3f(y*2.0f, -2.0f, -x*2.0f+2.0f);
						GL11.glVertex3f(y*2.0f+2.0f, -2.0f, -x*2.0f+2.0f);
						GL11.glVertex3f(y*2.0f+2.0f, -2.0f, -x*2.0f);
					GL11.glEnd();
					GL11.glPopMatrix();
				}
			}
		}
		*/
	}
	
	public static BufferedImage loadMap(String location) {
		BufferedImage result = null;
		
		try {
			InputStream mapStream = PGameWindow.class.getClassLoader().getResourceAsStream(location);
			result = ImageIO.read(mapStream);
			//ImageIO.write(result, "png", new File("C:/Users/0eevv/Desktop/troll.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (result == null)
			System.out.println("NULL!!");
		
		return result;
	}
}
