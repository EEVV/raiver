package purificiency.display;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import purificiency.camera.Camera;
import purificiency.fonts.FontHandler;
import purificiency.input.InputHandler;
import purificiency.map.Map;
import purificiency.map.MapLoader;

public class PGameWindow {	
	long lastFrame;
	int fps;
	long lastFPS;
	String resolution;
	
	FontHandler text = new FontHandler();
	
	public static Map currentMap = new Map(MapLoader.loadMap("purificiency/assets/map4.png"));
	
	public PGameWindow(String resolution) {
		this.resolution = resolution;
	}
	
	public void run() {
		try {
			String[] res = resolution.split("x");
			DisplayMode displayMode = null;
	        DisplayMode[] modes = Display.getAvailableDisplayModes();

	         for (int i = 0; i < modes.length; i++)
	         {
	             if (modes[i].getWidth() == 800
	             && modes[i].getHeight() == 600
	             && modes[i].isFullscreenCapable())
	               {
	                    displayMode = modes[i];
	               }
	         }
			//Display.setFullscreen(true);
			Display.setDisplayMode(displayMode);//Integer.parseInt(res[0]), Integer.parseInt(res[1])));
			Display.setFullscreen(false);
			//Display.setVSyncEnabled(true);
			//Display.sync(60);
			Display.create();
			res = null;
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(90.0f, (float) Display.getWidth() / (float) Display.getHeight(), 0.001f, 100.0f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(0.25f, 0.5f, 1.0f, 1.0f);
		
		getDelta();
		lastFPS = getTime();
		
		init();
		
		while (!Display.isCloseRequested()) {
			int delta = getDelta();
			update(delta);
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			
			//FontHandler.loadFontText();
			//text.drawText("123", 0, 0, 1.0f);
			
			
			//MapLoader.render();
			GL11.glPushMatrix();
				GL11.glRotatef(Camera.getRX(), 1.0f, 0.0f, 0.0f);
				GL11.glRotatef(Camera.getRY(), 0.0f, 1.0f, 0.0f);
				GL11.glRotatef(Camera.getRZ(), 0.0f, 0.0f, 1.0f);
				GL11.glTranslatef(-Camera.getX(), -Camera.getY(), -Camera.getZ());
				currentMap.render();
			GL11.glPopMatrix();
			
			
			Display.update();
			//Display.sync(60);
		}
		Display.destroy();
	}
	
	public void update(int delta) {
		InputHandler.update(delta);
		updateFPS();
	}
	public void init() {
		FontHandler.loadFont("purificiency/assets/font.png");
		Map.init();
		//MapLoader.loadMap("purificiency/assets/map1.png");
		//Map 
	}
	
	public int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;
		
		return delta;
	}
	
	public long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("Purificiency fps: "+fps+" Cam x:"+Camera.getX()+" Cam z:"+Camera.getZ());
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}
}
