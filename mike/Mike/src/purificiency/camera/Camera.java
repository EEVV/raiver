package purificiency.camera;

import purificiency.math.Vector3f;

public class Camera {
	
	private static Vector3f position = new Vector3f(7.0f, 1.75f, -15.0f);
	private static Vector3f rotation = new Vector3f();
	
	public static void translate(float x, float y, float z) {
		position.x += x;
		position.y += y;
		position.z += z;
	}
	
	public static void moveLoc(float x, float y, float z) {
		
	}
	
	public static void move(float x, float y, float z) {
		position.x = x;
		position.y = y;
		position.z = z;
	}
	
	public static void rotate(float x, float y, float z) {
		if (rotation.x+x < 90.0f) {// && rotation.x+x > -90.0f)
			rotation.x += x;
			if (rotation.x+x > -90.0f)
				rotation.x += x;
			else
				rotation.x = -90.0f;
		} else
			rotation.x = 90.0f;
		rotation.y += y;
		rotation.z += z;
	}
	
	public static float getX() {
		return position.x;
	}
	public static float getY() {
		return position.y;
	}
	public static float getZ() {
		return position.z;
	}
	
	public static float getRX() {
		return rotation.x;
	}
	public static float getRY() {
		return rotation.y;
	}
	public static float getRZ() {
		return rotation.z;
	}
}
