package purificiency.fonts;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import purificiency.map.MapLoader;

public class FontHandler {
	
	static BufferedImage currentFont = null;
	
	public static void loadFont(String location) {
		currentFont = MapLoader.loadMap(location);
	}
	
	public static void loadFontText() {
		int font = loadTexture(currentFont);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
	
	private static int loadTexture(BufferedImage image) { 
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
		
		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);

		for (int y = 0; y<image.getHeight(); y++) {
			for (int x = 0; x<image.getWidth(); x++) {
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xff));
				buffer.put((byte) ((pixel >> 8) & 0xff));
				buffer.put((byte) (pixel & 0xff));
				buffer.put((byte) ((pixel >> 24) & 0xFF)); 
			}
		}
		
		buffer.flip();
		
		int textureID = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);//GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);//GL12.GL_CLAMP_TO_EDGE);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
		
		return textureID;
	}
	
	public void drawText(String text, float x, float y, float size) {
		for (int i = 0; i<text.length(); i++) {
			byte cchar = (byte) text.charAt(i);
			GL11.glPushMatrix();
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
				GL11.glScalef(size, size, size);
				GL11.glTranslatef(x+i*0.5f, y, -1.0f);
				GL11.glEnable(GL11.GL_TEXTURE_2D);
				GL11.glBegin(GL11.GL_QUADS);
					GL11.glTexCoord2f((66.0f*5.0f)/512.0f, 0.0f);
					//GL11.glTexCoord2f((cchar-32.0f)*5/512.0f, 0.0f);
					GL11.glVertex3f(-0.25f, -0.25f, 0.0f);
					GL11.glTexCoord2f((66.0f*5.0f)/512.0f+(5.0f/512.0f), 0.0f);
					//GL11.glTexCoord2f((cchar-32.0f)*5/512.0f+5.0f/512.0f, 0.0f);
					GL11.glVertex3f(0.25f, -0.25f, 0.0f);
					GL11.glTexCoord2f((66.0f*5.0f)/512.0f+5.0f/512.0f, 1.0f);
					//GL11.glTexCoord2f((cchar-32.0f)*5/512.0f+5.0f/512.0f, 1.0f);
					GL11.glVertex3f(0.25f, 0.25f, 0.0f);
					GL11.glTexCoord2f((66.0f*5.0f)/512.0f, 1.0f);
					//GL11.glTexCoord2f((cchar-32.0f)*5/512.0f, 1.0f);
					GL11.glVertex3f(-0.25f, 0.25f, 0.0f);
				GL11.glEnd();
			GL11.glPopMatrix();
		}
	}
}
