package purificiency.map;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import purificiency.camera.Camera;

public class Map {

	private byte[][][] oldTiles;
	private byte[][][] floorTiles;
	public List<Integer> frontWallTiles = new ArrayList<Integer>();
	public List<Integer> rightWallTiles = new ArrayList<Integer>();
	public List<Integer> backWallTiles = new ArrayList<Integer>();
	public List<Integer> leftWallTiles = new ArrayList<Integer>();
	
	public boolean placelight = false;
	public boolean bleedlight = false;
	//private List<Integer> wallTiles = new ArrayList<Integer>();
	//private List<Integer> wallTilesBottom = new ArrayList<Integer>();
	//private List<Integer> wallTilesTop = new ArrayList<Integer>();
	
	static BufferedImage currentTexture = MapLoader.loadMap("purificiency/assets/tiles/woodenboards.png");
	
	static int textureID;
	
	public Map(BufferedImage mapImage) {
		generateMap(mapImage);
		generateWalls();
	}
	
	public Map() {
		
	}
	
	public static void init() {
		textureID = loadTexture(currentTexture);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
	
	public void generateMap(BufferedImage mapImage) {
		
		floorTiles = new byte[mapImage.getWidth()][mapImage.getHeight()][3];
		
		for (int x = 0; x<mapImage.getWidth(); x++) { // 124 93 83
			for (int y = 0; y<mapImage.getHeight(); y++) {
				Color currentColor = new Color(mapImage.getRGB(x, y));
				//System.out.println(Integer.toHexString(currentColor.getRGB()));
				
				
				
				switch (currentColor.getRGB()) {
				case (0xfffe0000): // 254, 0, 0 SPAWN
					Camera.move(x*2.0f, 1.75f, -y*2.0f);
					floorTiles[x][y][0] = 1;
					break;
				case (0xffffff00): // 255, 255, 0 LIGHT
					floorTiles[x][y][0] = 2;
					break;
				case (0xff010000): // 1, 0, 0 // WOOD
					floorTiles[x][y][0] = 1;
					break;
				default:
					floorTiles[x][y][1] = 0;
					break;
				}
				
			}
		}
	}
	
	public void generateWalls() {
		
		for (int x = 0; x<floorTiles.length; x++) {
			for (int y = 0; y<floorTiles[0].length; y++) {
				// Current pixel
				try {
					if (floorTiles[x][y][0] != 0) {
						floorTiles[x][y][1] = 0;						
						
						// 1 vertices (corners)
						// bottom left
						if (floorTiles[x-1][y-1][0] == 0)
							floorTiles[x][y][1] = 4;
						// bottom right
						if (floorTiles[x+1][y-1][0] == 0)
							floorTiles[x][y][1] = 3;
						// top right
						if (floorTiles[x+1][y+1][0] == 0)
							floorTiles[x][y][1] = 2;
						// top left
						if (floorTiles[x-1][y+1][0] == 0)
							floorTiles[x][y][1] = 1;
						
						// 2 vertices
						// front pixel
						if (floorTiles[x][y+1][0] == 0) {
							floorTiles[x][y][1] = 5;
							frontWallTiles.add(x*2);
							frontWallTiles.add(y*2);
						}
						// Right pixel
						if (floorTiles[x+1][y][0] == 0) {
							floorTiles[x][y][1] = 6;
							rightWallTiles.add(x*2+2);
							rightWallTiles.add(y*2);
						}
						// back pixel
						if (floorTiles[x][y-1][0] == 0) {
							floorTiles[x][y][1] = 7;
							backWallTiles.add(x*2);
							backWallTiles.add(y*2-2);
						}
						// Left pixel
						if (floorTiles[x-1][y][0] == 0) {
							floorTiles[x][y][1] = 8;
							leftWallTiles.add(x*2);
							leftWallTiles.add(y*2);
						}
						
						// 3 Vertices
						// Bottom right
						if (floorTiles[x][y-1][0] == 0 && floorTiles[x+1][y][0] == 0)
							floorTiles[x][y][1] = 10;
						// Top right
						if (floorTiles[x][y+1][0] == 0 && floorTiles[x+1][y][0] == 0)
							floorTiles[x][y][1] = 9;
						// Top left
						if (floorTiles[x][y+1][0] == 0 && floorTiles[x-1][y][0] == 0)
							floorTiles[x][y][1] = 12;
						// Bottom left
						if (floorTiles[x][y-1][0] == 0 && floorTiles[x-1][y][0] == 0)
							floorTiles[x][y][1] = 11;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					
				}
			}
		}
		//generateLightMap();
	}
	
	public void generateLightMap() {
		for (int x = 0; x<floorTiles.length; x++) {
			for (int y = 0; y<floorTiles[0].length; y++) {
					floorTiles[x][y][1] = 13;
			}
		}
		
		for (int x = 0; x<floorTiles.length; x++) {
			for (int y = 0; y<floorTiles[0].length; y++) {
				if (floorTiles[x][y][0] == 2) {
					
					for (int lx = 0; lx<floorTiles.length; lx++) {
						for (int ly = 0; ly<floorTiles.length; ly++) {
							int x1 = lx;
							int x0 = x;
							
							int y1 = ly;
							int y0 = y;
							
							short dx = (short) (Math.abs(x1-x0));
							short dy = (short) (Math.abs(y1-y0));
							short sx = (x0 < x1) ? (short) 1 : (short) -1;
							short sy = (y0 < y1) ? (short) 1 : (short) -1;
							short err = (short) (dx-dy);
							
							boolean dead = false;
							
							while (!dead) {
								if (floorTiles[x0][y0][0] == 0) {
									dead = true;
								} else {
									floorTiles[x0][y0][1] = 0;
								}
								
								if ((x0 == x1) && (y0==y1)) break;
								int e2 = 2*err;
								if (e2 > -dy) {
									err -= dy;
									x0 += sx;
								}
								if (e2 < dx) {
									err += dx;
									y0 += sy;
								}
							}
						}
					}					
				}
			}
		}
		
		if (bleedlight) {
			for (int x = 0; x<floorTiles.length; x++) {
				for (int y = 0; y<floorTiles[0].length; y++) {
					if (floorTiles[x][y][0] != 0 && floorTiles[x][y][1] != 13) {
						
						// Bottom left
						if (floorTiles[x-1][y-1][1] == 13)
							floorTiles[x][y][1] = 14;
						// Bottom right
						if (floorTiles[x+1][y-1][1] == 13)
							floorTiles[x][y][1] = 14;
						// Top right
						if (floorTiles[x+1][y+1][1] == 13)
							floorTiles[x][y][1] = 14;
						// Top left
						if (floorTiles[x-1][y+1][1] == 13)
							floorTiles[x][y][1] = 14;
						
						// Front
						if (floorTiles[x][y+1][1] == 13)// && floorTiles[x][y+1][0] != 0)
							floorTiles[x][y][1] = 14;
						// Left
						if (floorTiles[x-1][y][1] == 13)// && floorTiles[x-1][y][0] != 0)
							floorTiles[x][y][1] = 14;
						// Back
						if (floorTiles[x][y-1][1] == 13)// && floorTiles[x][y-1][0] != 0)
							floorTiles[x][y][1] = 14;
						// Right
						if (floorTiles[x+1][y][1] == 13)// && floorTiles[x+1][y][0] != 0)
							floorTiles[x][y][1] = 14;
						
					}
				}
			}
		}
	}
	
	private static int loadTexture(BufferedImage image) { 
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
		
		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);

		for (int y = 0; y<image.getHeight(); y++) {
			for (int x = 0; x<image.getWidth(); x++) {
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xff));
				buffer.put((byte) ((pixel >> 8) & 0xff));
				buffer.put((byte) (pixel & 0xff));
				buffer.put((byte) ((pixel >> 24) & 0xFF)); 
			}
		}
		
		buffer.flip();
		
		int textureID = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);//GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);//GL12.GL_CLAMP_TO_EDGE);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
		
		return textureID;
	}
	
	public void render() {
		if (placelight) {
			for (int x = 0; x<floorTiles.length; x++) {
				for (int y = 0; y<floorTiles[0].length; y++) {
					if (floorTiles[x][y][0] == 2)
						floorTiles[x][y][0] = 1;
				}	
			}
			floorTiles[Math.round(Camera.getX()/2.0f)][Math.round(-Camera.getZ()/2.0f)][0] = 2;
	
			generateLightMap();
		}
		
		
		for (int x = 0; x<floorTiles.length; x++) {
			for (int y = 0; y<floorTiles[0].length; y++) {
				switch (floorTiles[x][y][0]) {
				case 1:
					byte cond = floorTiles[x][y][1];
					
					GL11.glEnable(GL11.GL_TEXTURE_2D);
					GL11.glBegin(GL11.GL_QUADS);
						ambiance(0, cond);
						GL11.glTexCoord2f(0.0f, 0.0f);
						GL11.glVertex3i(x*2, 0, -y*2);
						ambiance(1, cond);
						GL11.glTexCoord2f(1.0f, 0.0f);
						GL11.glVertex3i(x*2+2, 0, -y*2);
						ambiance(2, cond);
						GL11.glTexCoord2f(1.0f, 1.0f);
						GL11.glVertex3i(x*2+2, 0, -y*2+2);
						ambiance(3, cond);
						GL11.glTexCoord2f(0.0f, 1.0f);
						GL11.glVertex3i(x*2, 0, -y*2+2);

						ambiance(0, cond);
						GL11.glTexCoord2f(0.0f, 0.0f);
						GL11.glVertex3i(x*2, 3, -y*2);
						ambiance(1, cond);
						GL11.glTexCoord2f(1.0f, 0.0f);
						GL11.glVertex3i(x*2+2, 3, -y*2);
						ambiance(2, cond);
						GL11.glTexCoord2f(1.0f, 1.0f);
						GL11.glVertex3i(x*2+2, 3, -y*2+2);
						ambiance(3, cond);
						GL11.glTexCoord2f(0.0f, 1.0f);
						GL11.glVertex3i(x*2, 3, -y*2+2);
					GL11.glEnd();
					break;
				/*
				default:
					GL11.glColor3f(0.5f, 0.5f, 0.5f);
					GL11.glBegin(GL11.GL_QUADS);
						GL11.glVertex3i(x*2, -2, -y*2);
						GL11.glVertex3i(x*2+2, -2, -y*2);
						GL11.glVertex3i(x*2+2, -2, -y*2+2);
						GL11.glVertex3i(x*2, -2, -y*2+2);
					GL11.glEnd();
					break;
				*/
				}
			}
		}
		GL11.glColor3f(0.5f, 0.5f, 0.5f);
		for (int i = 0; i<frontWallTiles.size(); i+=2) {
			GL11.glColor3f(0.6f, 0.5f, 0.5f);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glColor3f(0.5f, 0.6f, 0.7f);
				GL11.glVertex3i(frontWallTiles.get(i), 0, -frontWallTiles.get(i+1));
				GL11.glVertex3i(frontWallTiles.get(i)+2, 0, -frontWallTiles.get(i+1));
				GL11.glColor3f(0.6f, 0.5f, 0.5f);
				GL11.glVertex3i(frontWallTiles.get(i)+2, 3, -frontWallTiles.get(i+1));
				GL11.glVertex3i(frontWallTiles.get(i), 3, -frontWallTiles.get(i+1));
			GL11.glEnd();
		}
		for (int i = 0; i<rightWallTiles.size(); i+=2) {
			GL11.glColor3f(0.6f, 0.6f, 0.5f);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glColor3f(0.3f, 0.3f, 0.25f);
				GL11.glVertex3i(rightWallTiles.get(i), 0, -rightWallTiles.get(i+1));
				GL11.glVertex3i(rightWallTiles.get(i), 0, -rightWallTiles.get(i+1)+2);
				GL11.glColor3f(0.6f, 0.6f, 0.5f);
				GL11.glVertex3i(rightWallTiles.get(i), 3, -rightWallTiles.get(i+1)+2);
				GL11.glVertex3i(rightWallTiles.get(i), 3, -rightWallTiles.get(i+1));
			GL11.glEnd();
		}
		for (int i = 0; i<backWallTiles.size(); i+=2) {
			GL11.glColor3f(0.6f, 0.6f, 0.6f);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glColor3f(0.3f, 0.3f, 0.3f);
				GL11.glVertex3i(backWallTiles.get(i)+2, 0, -backWallTiles.get(i+1));
				GL11.glVertex3i(backWallTiles.get(i), 0, -backWallTiles.get(i+1));
				GL11.glColor3f(0.6f, 0.6f, 0.6f);
				GL11.glVertex3i(backWallTiles.get(i), 3, -backWallTiles.get(i+1));
				GL11.glVertex3i(backWallTiles.get(i)+2, 3, -backWallTiles.get(i+1));
			GL11.glEnd();
		}
		for (int i = 0; i<leftWallTiles.size(); i+=2) {
			GL11.glColor3f(0.5f, 0.6f, 0.5f);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glColor3f(0.25f, 0.3f, 0.25f);
				GL11.glVertex3i(leftWallTiles.get(i), 0, -leftWallTiles.get(i+1));
				GL11.glVertex3i(leftWallTiles.get(i), 0, -leftWallTiles.get(i+1)+2);
				GL11.glColor3f(0.5f, 0.6f, 0.5f);
				GL11.glVertex3i(leftWallTiles.get(i), 3, -leftWallTiles.get(i+1)+2);
				GL11.glVertex3i(leftWallTiles.get(i), 3, -leftWallTiles.get(i+1));
			GL11.glEnd();
		}
	}
	
	public void ambiance(int vertex, byte condition) {
		switch (condition) {
		case 0:
			GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 1:
			if (vertex == 0)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 2:
			if (vertex == 1)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 3:
			if (vertex == 2)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 4:
			if (vertex == 3)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 5:
			if (vertex == 0 || vertex == 1)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 6:
			if (vertex == 1 || vertex == 2)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 7:
			if (vertex == 2 || vertex == 3)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 8:
			if (vertex == 3 || vertex == 0)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 9:
			if (vertex == 0 || vertex == 1 || vertex == 2)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 10:
			if (vertex == 1 || vertex == 2 || vertex == 3)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 11:
			if (vertex == 2 || vertex == 3 || vertex == 0)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 12:
			if (vertex == 3 || vertex == 0 || vertex == 1)
				getAmbianceShadeColor();
			else
				GL11.glColor3f(1.0f, 1.0f, 1.0f);
			break;
		case 13:
			getAmbianceShadeColor();
			break;
		case 14:
			GL11.glColor3f(0.80f, 0.80f, 0.85f);
			break;
		}
	}
	
	private static final void getAmbianceShadeColor() {
		GL11.glColor3f(0.60f, 0.60f, 0.65f);
	}
	private static final void getAmbianceColor() {
		GL11.glColor3f(1.0f, 1.0f, 1.0f);
	}
	/*
	private static final int getColorID(byte id) {
		switch (id) {
		case 1:
			return 0x01010101;
		default:
			return 0x00000000;
		}
	}
	*/
}